import telebot, json, pymysql, re
from pymysql.cursors import DictCursor
from enum import Enum
from datetime import datetime

#Загрузка конфигурации
with open('config.json', 'r') as f:
    CONFIG = json.load(f)

#Подключаем бота
bot = telebot.TeleBot(CONFIG['TELEGRAM']['TOKEN'])
markup = telebot.types.ReplyKeyboardMarkup(1)
markup.row("/balance", "/history")
markup.row("/help")

class OperationType(Enum):
    BALANCE     = 0
    ARRIVAL     = 1
    CONSUMPTION = 2
    HISTORY     = 3

def get_db():
    return pymysql.connect(
        host     = CONFIG['DB']['HOST'],
        user     = CONFIG['DB']['USER'],
        password = CONFIG['DB']['PASSWORD'],
        db       = CONFIG['DB']['DBNAME'],
        charset  = CONFIG['DB']['CHARSET'],
        cursorclass = DictCursor
    )

def insert_operation(user, optype, opval, opcomment):
    db = get_db()
    cursor = db.cursor()

    query = "CALL %s('%s', %i, %f, '%s')" % (
        CONFIG['DB']['FUNCS']['ADD_OPERATION'],
        user,
        optype,
        opval,
        opcomment
    )
    cursor.execute(query)

    cursor.close()
    db.close()

def get_users():
    db = get_db()
    cursor = db.cursor()

    query = "SELECT * FROM %s" % CONFIG['DB']['TABLES']['USERS']
    cursor.execute(query)

    users = cursor.fetchall()

    cursor.close()
    db.close()

    return users

def get_balance_value(user):
    db = get_db()
    cursor = db.cursor()

    query = "SELECT val, type FROM %s WHERE type in(%i, %i)" % (
        CONFIG['DB']['TABLES']['OPERATIONS'],
        OperationType.ARRIVAL,
        OperationType.CONSUMPTION
    )
    cursor.execute(query)

    balance = 0

    for row in cursor:
        if row['optype'] == OperationType.ARRIVAL:
            balance += row['opval']
        else:
            balance -= row['opval']

    #Добавляем информацию о получении балансов
    query = "CALL %s('%s')" % (CONFIG['DB']['FUNCS']['BALANCE'], user)
    cursor.execute(query)

    cursor.close()
    db.close()

    return balance

def get_history_value(user):
    db = get_db()
    cursor = db.cursor()

    query = "SELECT * FROM %s ORDER BY date DESC" % (CONFIG['DB']['TABLES']['OPERATIONS'])
    cursor.execute(query)
    result = cursor.fetchall()

    #Добавляем информацию о получении истории
    query = "CALL %s('%s')" % (CONFIG['DB']['FUNCS']['HISTORY'], user)
    cursor.execute(query)

    cursor.close()
    db.close()

    return result

@bot.message_handler(commands = CONFIG['TELEGRAM']['COMMANDS']['HELP'])
def get_help(msg):
    bot.send_message(
        msg.from_user.id,
        CONFIG['TELEGRAM']['TEXTS']['HELP'],
        reply_markup=markup
    )

@bot.message_handler(commands = CONFIG['TELEGRAM']['COMMANDS']['BALANCE'])
def get_balance(msg):
    bot.send_message(
        msg.from_user.id,
        CONFIG['TELEGRAM']['TEXTS']['BALANCE'] % get_balance_value(msg.from_user.id),
        reply_markup=markup
    )

@bot.message_handler(commands = CONFIG['TELEGRAM']['COMMANDS']['HISTORY'])
def get_history(msg):
    users = get_users()
    history = get_history_value(msg.from_user.id)

    datenow = datetime.strftime(datetime.now(), "%d.%m.%Y %H:%M:%S")
    send_text = CONFIG['TELEGRAM']['TEXTS']['HISTORY_HEAD'] + '\n' % datenow

    for row in history:
        username = users[row['user']]
        if row['type'] == OperationType.BALANCE:
            send_text += CONFIG['TEXTS']['OPERATIONS']['BALANCE'] + '\n' % username
        elif row['type'] == OperationType.HISTORY:
            send_text += CONFIG['TEXTS']['OPERATIONS']['HISTORY'] + '\n' % username
        elif row['type'] == OperationType.ARRIVAL:
            send_text += CONFIG['TEXTS']['OPERATIONS']['ARRIVAL'] % (
                username,
                row['val'],
                row['date'],
                row['comment']
            ) + '\n'
        elif row['type'] == OperationType.CONSUMPTION:
            send_text += CONFIG['TEXTS']['OPERATIONS']['CONSUMPTION'] % (
                username,
                row['val'],
                row['date'],
                row['comment']
            ) + '\n'

    bot.send_message(
        msg.from_user.id,
        send_text,
        reply_markup=markup
    )

@bot.message_handler(commands = CONFIG['TELEGRAM']['COMMANDS']['ARRIVAL'])
def add_arrival(msg):
    args = re.search(r'\/\S+\s+(\S+)\s+(.+)$', msg)
    insert_operation(msg.from_user.id, OperationType.ARRIVAL, args.group(1), args.group(2))
    bot.send_message(
        msg.from_user.id,
        CONFIG['TELEGRAM']['TEXTS']['DONE']['ARRIVAL'],
        reply_markup=markup
    )

@bot.message_handler(commands = CONFIG['TELEGRAM']['COMMANDS']['CONSUMPTION'])
def add_consumtion(msg):
    args = re.search(r'\/\S+\s+(\S+)\s+(.+)$', msg)
    insert_operation(msg.from_user.id, OperationType.CONSUMPTION, args.group(1), args.group(2))
    bot.send_message(
        msg.from_user.id,
        CONFIG['TELEGRAM']['TEXTS']['DONE']['CONSUMPTION'],
        reply_markup=markup
    )

@bot.message_handler(content_types=['text'])
def get_text_message(msg):
    bot.send_message(
        msg.from_user.id,
        CONFIG['TELEGRAM']['TEXTS']['UNKNOWN'],
        reply_markup=markup
    )

bot.polling(none_stop=True, interval=0)